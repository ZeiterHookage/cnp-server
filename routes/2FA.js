const express = require('express');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const uuid=require('uuid');
const JsonDB = require('node-json-db').JsonDB;
const Config = require('node-json-db/dist/lib/JsonDBConfig').Config;
const bodyParser = require('body-parser');


const db = new JsonDB(new Config('myDatabase',true,false,'/'))


const app = express();
//app.use(express.json())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.post('/2fa', (req, res) => {
    const id= uuid.v4();
    const path=`/user/${id}`;
    const temp_secret= speakeasy.generateSecret({length:20})
    db.push(path,{id,temp_secret})
    // console.log(token);
    QRCode.toDataURL(temp_secret.otpauth_url, function(err, image_data) {
        // A data URI for the QR code image
        if (err) {
             res.status(400).json({
                ok: false,
                err
            })
        }


        
         res.json({
            id,
            message: '2FA Auth needs to be verified',
            tempSecret: temp_secret.base32,
            image_data,
            tfaURL: temp_secret.otpauth_url

        });

    });



});

app.post('/2faVerify',(req,res)=>{
    const{sms,userId} =req.body
 console.log('2/faVerify',sms,userId);
    try{
        const path=`/user/${userId}`
        const user = db.getData(path)
        const {base32:secret}= user.temp_secret
        const verified= speakeasy.totp.verify({secret,
     encoding:'base32',
     sms
 });
 
 if(verified){
     db.push(path,{id:userId,secret:user.temp_secret})
     res.json({verified:true})
 }else{
     res.json({verified:false})
 }
    }catch(error){
 console.log(error)
 res.status(500).json({message:'Error finding user'})
    }
 })
 
 
 app.post('/2faValidate',(req,res)=>{
    const{sms,userId} =req.body
 
    try{
        const path=`/user/${userId}`
        const user = db.getData(path)
        const {base32:secret}= user.secret
        const tokenValidates= speakeasy.totp.verify({secret,
     encoding:'base32',
     sms,
     window:1
 });
 
 if(tokenValidates){
     res.json({validated:true})
 }else{
     res.json({validated:false})
 }
    }catch(error){
 console.log(error)
 res.status(500).json({message:'Error finding user'})
    }
 })

module.exports = app;