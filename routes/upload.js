const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const User= require('../models/user');
const fs= require('fs');
const path=require('path');
// default options
app.use(fileUpload());
app.put('/upload/:tipe/:id', function(req, res){

  let tipe = req.params.tipe;
  let id =req.params.id;

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok:false,
            err:{
                message:'No files were uploaded here.'
            }
        });
      }

      let validType=['documents','peopleImage']
      if(validType.indexOf(tipe)<0)
      {
        return res.status(400).json({
         ok:false,
         err:{
           message:'the Extention it is no allow , Just allow extention like ' + validType.join(', ')
           
         } 
        })
      }

      let file=req.files.file;
      let fileName= file.name.split('.');
      let extent = fileName[fileName.length -1];


      
      // allow extended's files

      let validExtention=['png','jpg','gif','jpeg','svg'];
      if(validExtention.indexOf(extent) < 0)
      {
        return res.status(400).json({
         ok:false,
         err:{
           message:'the Extention it is no allow , Just allow extention like ' + validExtention.join(', ')
         } 
        })
      }
//changing the name of the file;
  let fileNames= `${id}-${new Date().getMilliseconds()}.${extent}`;
      
     // uploadPath = __dirname + '/somewhere/on/your/server/' + sampleFile.name;
     //uploadPath = path.resolve(__dirname, + `/uploads/${ archivo }/${ tipo }`);
  // Use the mv() method to place the file somewhere on your server
  file.mv(`uploads/${tipe}/${fileNames}`, (err) =>{
    if (err){
      return res.status(500).json({
          ok:false,
         err
        
        });
    }
      
   UserImage(id,res,fileNames);
   
  });
})

function UserImage(id,res,fileNames){
User.findById(id,(err,userDb)=>{
  if(err){
    deleteImg(fileNames,'peopleImage');
    return res.status(500).json({
      ok:false,
      err

    })
  }
  if(!userDb){
    deleteImg(fileNames,'peopleImage');
    return res.status(400).json({
      ok:false,
      err:{
        message:"user not found"
      }

    })
  }
  /*let pathImg=pat.resolve(__dirname,` ../../uploads/peopleImage/${userDb.img}`) ;

  if(fs.existsSync(pathImg))
  {
    fs.unlinkSync(pathImg)
  }*/
  deleteImg(userDb.img,'peopleImage')
userDb.img = fileNames;
userDb.save((err ,userSaved=>{
res.json({
  ok:true,
  user:userSaved,
  img:fileNames
})
}))
})

function deleteImg(nameImg,validType){
  let pathImg=path.resolve(__dirname,`../uploads/${validType}/${nameImg}`) ;

 if(fs.existsSync(pathImg))
 {
   fs.unlinkSync(pathImg);
 }
}

}
module.exports= app;