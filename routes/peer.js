const express = require('express');
const app = express();
const { io } = require('../server/server');
const RTCMultiConnectionServer = require('rtcmulticonnection-server');
const Turn = require('node-turn');

var server = new Turn({
    // set options
    authMech: 'long-term',
    credentials: {
        username: "password"
    }
});
server.start();





io.on('connection', socket => {

    RTCMultiConnectionServer.addSocket(socket, config);

    const jsonPath = {
        config: 'server/config.json',
        logs: 'server/logs.json'
    };

    const BASH_COLORS_HELPER = RTCMultiConnectionServer.BASH_COLORS_HELPER;
    const getValuesFromConfigJson = RTCMultiConnectionServer.getValuesFromConfigJson;
    const getBashParameters = RTCMultiConnectionServer.getBashParameters;

    var config = getValuesFromConfigJson(jsonPath);
    config = getBashParameters(config, BASH_COLORS_HELPER);

    RTCMultiConnectionServer.ScalableBroadcast = true;


    const params = socket.handshake.query;

    if (!params.socketCustomEvent) {
        params.socketCustomEvent = 'custom-message';
    }

    socket.on(params.socketCustomEvent, function(message) {
       
        socket.broadcast.emit(params.socketCustomEvent, message);
    });

    

})




module.exports = app;