const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');


let Schema = mongoose.Schema;

let ValidRolle = {
    values: ['ADMIN_ROLE', 'USER_ROLE', 'HHRR_ROLE', 'SVISION_ROLE', 'TEACHER_ROLE'],
    message: '{VALUE} Ese no es un Rolle valido x_X :('
}

let UserSchema = new Schema({
    firstname: {
        type: String,
        required: [true, 'el Nombre es obligatorio']

    },
    lastname: {
        type: String,
        required: [true, 'el apellido es obligatorio']

    },
    password: {
        type: String,
        required: [true, 'la contraseña es obligatoria']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'el correo es obligatorio']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: ValidRolle

    },
    state: {
        type: Boolean,
        default: false
    },
    google: {
        type: Boolean,
        default: false
    },
    address: {
        type: String,
        required: [true, 'el correo es obligatorio'],
    },
    city: {
        type: String,
        required: [true, ' la ciudad es obligatoria'],
    },
    telephone: {
        type: Number,
        required: false,
    },
    timeWork: {
        type: String,
        required: false,
    },
    schoolName: {
        type: String,
        required: [true, 'le Nombre de la Escuela  es obligatorio'],
    },
    grade: {
        type: String,
        required: [true, 'el grado  es obligatorio'],
    },
    roomLetter: {
        type: String,
        required: [true, 'el el curso es obligatorio'],
    },
    agree: {
        type: Boolean,
        default: false,
        required: [true, 'el Debe de estar de acuerdo con nuestras politicas de uso']
    },


});
UserSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
}
UserSchema.plugin(uniqueValidator, { message: '{PATH} debe ser unico' });

module.exports = mongoose.model('UserCNP', UserSchema);