"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Users =
/*#__PURE__*/
function () {
  function Users() {
    _classCallCheck(this, Users);

    this.people = [];
  }

  _createClass(Users, [{
    key: "addPeople",
    value: function addPeople(id, name, room) {
      var person = {
        id: id,
        name: name,
        room: room,
        date: new Date().getTime()
      };
      this.people.push(person);
      return this.people;
    }
  }, {
    key: "getPerson",
    value: function getPerson(id) {
      var person = this.people.filter(function (person) {
        return person.id === id;
      })[0];
      return person;
    }
  }, {
    key: "getPeople",
    value: function getPeople() {
      return this.people;
    }
  }, {
    key: "getPersonForRoom",
    value: function getPersonForRoom(room) {
      var peopleInRoom = this.people.filter(function (person) {
        return person.room === room;
      });
      return peopleInRoom;
    }
  }, {
    key: "deletePerson",
    value: function deletePerson(id) {
      var deletePerson = this.getPerson(id);
      this.people = this.people.filter(function (person) {
        return person.id !== id;
      });
      return deletePerson;
    }
  }]);

  return Users;
}();

module.exports = {
  Users: Users
};