class Users {
    constructor() {
        this.people = [];

    }
    addPeople(id, name, room,img) {
        
        let person = {
            id,
            name,
            room,
            img,
            date: new Date().getTime()
        };

        this.people.push(person)
        return this.people ;
    }

    getPerson(id) {
        let person = this.people.filter((person) => person.id === id)[0];

        return person;
    }

    getPeople() {
        return this.people;
    }
    getPersonForRoom(room) { 
        let hash = {};
        let peopleInRoom = this.people.filter(person => person.room === room);
        peopleInRoom = peopleInRoom.filter((current)=> {
           
            let exists = !hash[current.id];
          hash[current.id] = true;
            return exists;
          });
        return peopleInRoom;

    }
    deletePerson(id) {

        let deletePerson = this.getPerson(id);

        this.people = this.people.filter(person => person.id !== id);

        return deletePerson;
    }

}

module.exports = {
    Users
};