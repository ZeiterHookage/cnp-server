//entviroment variable
require('../config/config');

const express = require('express');
//const { ExpressPeerServer } = require('peer');
const app = express();
module.exports.RTCMultiConnectionServer = require('rtcmulticonnection-server');
const path = require('path');
const socketIO = require('socket.io');
const csrfProtection = require('@authentication/csrf-protection');
const http = require('http');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');


//implementing socket 
const server = http.createServer(app);
//const serverApp = http.createServer(serverHandler);

// implementing socket 
module.exports.io = socketIO(server);
require('./sockets/socket.js');
//require('../middlewares/Authentication');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//again atack csrf
//app.use(csrfProtection());


//cors
app.use(cors({ origin: true, credentials: true }));




//const publicPath = path.resolve(__dirname, '../public');
//app.use(express.static(publicPath));





// database MongodB CNPMongodb
mongoose.connect('mongodb://localhost:27017/CNPMongodb', function(err, resp) {
    if (err) throw err;

    console.log("CMP mongo is alive!!!!");

});

app.use(require('../routes/peer'));
app.use(require('../routes/index'));
const servers = server || serverApp
 
server.listen(process.env.PORT || PORT, process.env.IP || "0.0.0.0", (err) => {
    if (err) throw new Error(err);
    console.log(`Servidor corriendo en el puerto : ${process.env.PORT}`);
 
});