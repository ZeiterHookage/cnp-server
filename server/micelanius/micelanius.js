const createMessage = (name, message, mySelf, joined,id,img) => {
    if (name !== undefined) {
        return {
            name,
            message,
            mySelf,
            other: !mySelf,
            joined,
            id,
            img,
            date: new Date().getTime()
        }
    }
    return;
}


module.exports = {
    createMessage
}