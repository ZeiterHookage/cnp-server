"use strict";

var createMessage = function createMessage(name, message, mySelf, joined) {
  return {
    name: name,
    message: message,
    mySelf: mySelf,
    other: !mySelf,
    joined: joined,
    date: new Date().getTime()
  };
};

var createMessageCallBACK = function createMessageCallBACK(name, message, mySelf) {
  return {
    name: name,
    message: message,
    mySelf: mySelf,
    date: new Date().getTime()
  };
};

module.exports = {
  createMessage: createMessage,
  createMessageCallBACK: createMessageCallBACK
};