const { io } = require("../server");
const { Users } = require("../classes/users");
const users = new Users();
const { createMessage } = require("../micelanius/micelanius");
io.on("connection", (client) => {




 client.on("enterToRoom", (data, callback) => {
        if (!data.name ) {
            return callback({
                error: true,
                message: "El nombre y la sala  son necesario",
            });
        }
        client.join(data.room);
        users.addPeople(client.id, data.name, data.room,data.img);
        client.broadcast
            .to(data.room)
            .emit("peopleList", users.getPersonForRoom(data.room));
        client.broadcast
            .to(data.room)
            .emit(
                "createMessage",
                createMessage("Administrador", `${data.name} regresó al Aula`, false, true,client.id,null)
            );
        callback(users.getPersonForRoom(data.room));

    
         
          
    });

    //creando mensaje publico
 client.on("createMessage", (data, callback) => {

    let person = users.getPerson(client.id);
        let message = createMessage(person.name, data.message, false,false,data.id,data.img);
        let messageCallBack = createMessage(person.name, data.message, true, true,data.id,data.img);
        client.broadcast.to(data.room).emit("createMessage", message);
      
        callback(messageCallBack);

      
    });
      


    // public emition
    client.on("disconnect", () => {

        let deletedPerson = users.deletePerson(client.id);
        if (deletedPerson === undefined) {
            return;
        }
        client.broadcast.to(deletedPerson['room']).emit('createMessage', createMessage('Administrador', `${deletedPerson['name']} salió del Aula`, false, false,client.id,null));
       
        client.broadcast.to(deletedPerson['room']).emit('peopleList', users.getPeople());
       

    client.on("privateMessage", ( data) =>{
      if(data !== undefined){
        let person = users.getPerson(client.id);
        client.broadcast
            .to(data.To)
            .emit("privateMessage", createMessage(person.name, data.message, data.mySelf, true,data.id,data.img));
      }
        
    });

  })
////
 client.on('isChat', async (ChatObj) =>{
      let obj = await ChatObj;
      console.log(obj);
            // #method2: this doesn't work though: all clients receive this event. Why?! Bug?
             client.broadcast.emit('isChat',obj).to(obj.room);
               
       
       
    });
    client.on('idChat', async (imgObj) =>{
      let obj = await imgObj;
      console.log('idChat',obj);
    
          client.broadcast.emit('idChat',obj).to(obj.room) ;
      
       
       
    });
client.on('isCamera', async(CameraObj) =>{
  let obj = await CameraObj;
  console.log(obj);
client.broadcast.emit("isCamera",obj).to(obj.room);

        });     
        
        //toolbar controller
  
   
 client.on('bBTitle', (title) =>{client.broadcast.emit("bBTitle",title);});
   
    // blackboard controller
        //title
client.on('update-user',  async (data, id)=> {
    id[id].emit('refresh', data);
  });



 client.on('isBlackboard', async(BlackBoardObj) =>{
      let obj = await BlackBoardObj;
    client.broadcast.emit("isBlackboard",obj).to(obj.room);
      
    
              });

client.on('path-created',  async (data)=> {
   let obj = await data;
    client.broadcast.emit('path-created', obj.canvas);
  });


  // Remove Obj
  client.on('remove',  async (data)=> {
    let obj = await data;
    console.log(obj);
    client.broadcast.emit('remove', obj.canvas);
  });




  // Add Obj
  /*client.on('add',  async (data)=> {
    let obj = await data;
    client.broadcast.emit('add', obj.canvas);
  });*/

  // Refresh All
  client.on('refresh',  async (data)=> {
    let obj = await data;
    client.broadcast.emit('refresh', obj);
  });

    //  ReSync All
  /*client.on('resync',  async (data)=> {
    let obj = await data;
    client.broadcast.emit('resync', obj.canvas);
  });*/


})

   

   


    

   
   











    

