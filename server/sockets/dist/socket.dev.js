"use strict";

var _require = require("../server"),
    io = _require.io;

var _require2 = require("../classes/users"),
    Users = _require2.Users;

var _require3 = require("../micelanius/micelanius"),
    createMessage = _require3.createMessage;

var users = new Users();
io.on("connection", function (client) {
  client.on("enterToRoom", function (data, callback) {
    if (!data.name) {
      return callback({
        error: true,
        message: "El nombre y la sala  son necesario"
      });
    }

    client.join(data.room);
    users.addPeople(client.id, data.name, data.room);
    client.broadcast.to(data.room).emit("peopleList", users.getPersonForRoom(data.room));
    client.broadcast.to(data.room).emit("createMessage", createMessage("Administrador", "".concat(data.name, " regres\xF3 al Aula"), false, true));
    callback(users.getPersonForRoom(data.room));
  }); //creando mensaje publico

  client.on("createMessage", function (data, callback) {
    var person = users.getPerson(client.id);
    var message = createMessage(person.name, data.message, false);
    var messageCallBack = createMessage(person.name, data.message, true, true);
    client.broadcast.to(data.room).emit("createMessage", message);
    callback(messageCallBack);
  }); // public emition

  client.on("disconnect", function () {
    var deletedPerson = users.deletePerson(client.id);

    if (deletedPerson === undefined) {
      return;
    }

    client.broadcast.to(deletedPerson['room']).emit('createMessage', createMessage('Administrador', "".concat(deletedPerson['name'], " sali\xF3 del Aula"), false, false));
    /*client.broadcast.emit(
        "createMessage",
        createMessage("Administrador", ` salió del Aula`)
    );*/

    client.broadcast.to(deletedPerson['room']).emit('peopleList', users.getPeople()); //client.broadcast.emit("peopleList", users.getPeople());
  }); //private Messge

  client.on("privateMessage", function (data) {
    var person = users.getPerson(client.id);
    client.broadcast.to(data.To).emit("privateMessage", createMessage(person.name, data.message, data.mySelf, true));
  }); //////////////////////
  //camera handler
  ///////////////////////////

  client.on('create or join', function (room) {
    console.log('create or join to room ', room);
    var myRoom = io.sockets.adapter.rooms[room] || {
      length: 0
    };
    var numCilents = myRoom.length;
    console.log(room, 'has', numCilents, 'clients');

    if (numCilents === 0) {
      client.join(room);
      client.emit('created', room);
      console.log('created', room);
    } else {
      client.join(room);
      client.emit('joined', room);
      console.log('joined', room);
    }
  });
  client.on('ready', function (room) {
    client.broadcast.to(room).emit('ready');
  });
  client.on('candidate', function (event) {
    client.broadcast.to(event['room']).emit('candidate', event);
  });
  client.on('offer', function (event) {
    client.broadcast.to(event['room']).emit('offer', event);
  });
  client.on('answer', function (event) {
    client.broadcast.to(event['room']).emit('answer', event);
  });
});