const jwt = require('jsonwebtoken');
const speakeasy = require('speakeasy');
const { io } = require('../server/server');

const JsonDB = require('node-json-db').JsonDB;
const Config = require('node-json-db/dist/lib/JsonDBConfig').Config;
const db = new JsonDB(new Config('myDatabase',true,false,'/'))
//check tokens
let verifyToken = (req, res, next) => {
    let token = req.get('Authorization');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'The Token is not valid'
                }
            })
        }
        req.user = decoded.user;
        next();
    })
};



// check roles // checks it
let verifyRole = (req, res, next) => {
    let user = req.user;
    if (user.role === 'ADMIN_ROLE') {
        next();
    } else {
        return res.json({
            ok: false,
            err: 'the User is not an Admin'
        })
    }
}


let Verify2FA=(req,res,next)=>{
    const{sms,userId} =req.body;
    console.log('Verify2FA',req.body)
    try{
        const path=`/user/${userId}`
        const aula = db.getData(path);
        const {base32:secret}= aula.temp_secret;
        console.log('secrect',secret);
        const verified= speakeasy.totp.verify({secret,
     encoding:'base32',
     token:sms
 });
 
 if(verified){ 
     console.log(true);
      next();
      ////////////
      // dataBase
      /////////////////
    // db.push(path,{id:userId,secret:aula.temp_secret});
     /*res.json({
         ok:true,
         email,
         password
        }) ;*/
   
 }else{
    console.log(false);
    // res.json({verified:false});
    return;
 }
    }catch(error){
 console.log(error)
 res.status(500).json({message:'Error finding aula'})
    }
 }
 
 /*let Validate2FA=('/2faValidate',(req,res,next)=>{
    const{token,userId} =req.body
 
    try{
        const path=`/user/${userId}`
        const user = db.getData(path)
        const {base32:secret}= user.secret
        const tokenValidates= speakeasy.totp.verify({secret,
     encoding:'base32',
     token,
     window:1
 });
 
 if(tokenValidates){
     res.json({validated:true});
     next()
 }else{
     res.json({validated:false})
 }
    }catch(error){
 console.log(error)
 res.status(500).json({message:'Error finding user'})
    }
    
 })*/
 let verifyTokenImg = (req, res, next) => {

    let token=req.query.token;
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'The Token is not valid'
                }
            })
        }
        req.user = decoded.user;
        next();
    })
    rest.json({
        token 
    })
 }


let VerifyCaptcha = (req, res, next) => {

    let token = {};
    let secrect = {};
    io.on('connection', (Client) => {

        Client.on('captcha', (captcha) => {

            console.log('captcha', captcha);

        });


    })

    next();
}


module.exports = {
    verifyToken,
    verifyRole,
    Verify2FA,
    VerifyCaptcha,
    verifyTokenImg
}